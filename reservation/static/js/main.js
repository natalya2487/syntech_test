/* datetimepicker for datetimeinput and dateinput */
function initFlatPickr() {
    $('input.datetimeinput').flatpickr({
        // firstDayOfWeek: 1,
        allowInput: true,
        time_24hr: true,
        enableTime: true,
        locale: 'ru',
        dateFormat: "d.m.Y"
    });

    flatpickr('input.dateinput', {
        allowInput: true,
        locale: 'ru',
        dateFormat: "d.m.Y"
    });

    flatpickr('input.datefromtoinput', {
        mode: 'range',
        allowInput: true,
        locale: 'ru',
        dateFormat: "d.m.Y"
    });
}

function initTableSelector() {
    $('#reservation_id select').change(function (event) {
        // console.log('select change');
        expand = $('#expand_id');
        expand.removeClass('d-none');
        $("p").removeClass("intro");
        return true;
    });
}

function selectTable(item) {
    var table_id = item.getAttribute('data-table_id');
    var placing_id = item.getAttribute('data-placing_id');
    el = $(item);

    if (item.classList.contains('available')){
        $('#div_id_table select').val(table_id);

        if (item.classList.contains('selected')) {
            el.removeClass('selected');
            $('#expand_id').addClass('d-none');
        }
        else {
            item.classList.add('selected');
            $('#expand_id').removeClass('d-none');
        }

    }
}

$(document).ready(function(){
    initTableSelector();
    initFlatPickr();
});
