from typing import Tuple
import os

from django.db import models
from django.conf import settings


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-
    updating `` created`` and ``modified`` fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Table(TimeStampedModel):
    """
    Tables
    """
    # shape.
    SHAPE_CHOICE_RECTANGLE = 'rectangle'
    SHAPE_CHOICE_ELLIPS = 'oval'
    SHAPE_CHOICES = (
        (SHAPE_CHOICE_RECTANGLE, "прямоугольный"),
        (SHAPE_CHOICE_ELLIPS, "овальный"),
    )

    table_number = models.IntegerField(verbose_name='Номер')
    number_of_seats = models.IntegerField(verbose_name='Количество мест')
    shape = models.CharField(verbose_name='Форма', choices=SHAPE_CHOICES, max_length=15)
    size_x = models.IntegerField(verbose_name='Ширина (%)')
    size_y = models.IntegerField(verbose_name='Длина (%)')

    def is_oval(self):
        return True if self.shape == self.SHAPE_CHOICE_ELLIPS else False

    def __str__(self):
        return "Стол №: {}, количество мест: {}, {}".format(self.table_number, self.number_of_seats, self.get_shape_display())

    class Meta:
        ordering = ('table_number',)
        verbose_name = "Стол"
        verbose_name_plural = "Столы"


class Hall(TimeStampedModel):
    name = models.CharField(max_length=50)

    def __str__(self):
        return 'Зал {}'.format(self.name)

    class Meta:
        ordering = ('name',)
        verbose_name = "Зал"
        verbose_name_plural = "Залы"


class Scheme(TimeStampedModel):

    date = models.DateField()
    hall = models.ForeignKey(Hall, verbose_name='Зал', on_delete=models.CASCADE)
    # layout = models.ImageField(verbose_name='Схема размещения столов', upload_to='layouts', null=True, blank=True)

    class Meta:
        unique_together = (('hall', 'date'),)
        verbose_name_plural = 'Схема размещения столов'
        ordering = ('date', 'hall',)

    @property
    def count_tables(self):
        return self.placing_set.count()

    def __str__(self):
        return "{}: Зал: {}, Кол-во столов: {}".format(self.date, self.hall.name, self.count_tables)


class Placing(TimeStampedModel):
    date = models.DateField()
    table = models.ForeignKey(Table, verbose_name='Стол', on_delete=models.DO_NOTHING)
    coord_x = models.IntegerField(verbose_name='Координаты X(%)')
    coord_y = models.IntegerField(verbose_name='Координаты Y(%)')
    scheme = models.ForeignKey(Scheme, verbose_name='Схема размещения столов', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('table', 'date'),)
        verbose_name_plural = 'Размещение'
        ordering = ('table__table_number', )

    def __str__(self):
        return "{}: Зал: {}, Cтол: {}".format(self.date, self.scheme.hall.name, self.table.table_number)


class Client(TimeStampedModel):
    name = models.CharField(verbose_name='Имя', max_length=50, null=True, blank=True)
    email = models.EmailField(verbose_name='Email', null=True, blank=True, unique=True)

    def __str__(self):
        return "{} - {}".format(self.name, self.email)

    class Meta:
        verbose_name_plural = 'Клиенты'
        verbose_name = 'Клиент'
        ordering = ('email',)


class Reservation(TimeStampedModel):
    date = models.DateField(verbose_name='Дата')
    placing = models.ForeignKey(Placing, verbose_name='Стол', on_delete=models.CASCADE, blank=True)
    client = models.ForeignKey(Client, verbose_name='Клиент', on_delete=models.CASCADE, default=None)

    @property
    def table(self):
        return self.placing.table

    def __str__(self):
        return '{}, Стол: {}, Имя: {}, Email: {}'.format(self.date, self.table.table_number, self.client.name, self.client.email)

    class Meta:
        unique_together = ('placing', 'date',)
        verbose_name_plural = 'Резервации столов'
