from django.db.utils import IntegrityError
from django.utils import timezone
from django.shortcuts import render
from django.views import generic as views
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from .models import Reservation, Client, Hall, Scheme, Placing
from .forms import ReservationForm, ClientForm, DateForm
from restaurant.settings import ADMIN_EMAIL


def index(request):
    halls = Hall.objects.all()
    date = timezone.now().date()
    schemes = Scheme.objects.filter(date=date)
    for hall in halls:
        scheme = Scheme.objects.get_or_create(hall=hall, date=date)
        hall.cur_scheme = scheme
    return render(request, 'reservation/index.html',
                  {'halls': halls,
                   'date': date.strftime('%Y-%m-%d'),
                   'schemes': schemes})


class SchemeTemplateView(views.TemplateView):
    model = Scheme
    template_name = 'reservation/base.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        hall = Hall.objects.get(pk=kwargs['pk'])
        cur_day = timezone.datetime.strptime(kwargs['date'], '%Y-%m-%d').date()

        today = timezone.now()

        # check if we need to display some specific day
        next_day = cur_day + timezone.timedelta(hours=24)
        prev_day = cur_day - timezone.timedelta(hours=24)

        context['cur_day'] = cur_day.strftime('%Y-%m-%d')
        context['today'] = today.strftime('%Y-%m-%d')
        context['prev_day'] = prev_day.strftime('%Y-%m-%d')
        context['next_day'] = next_day.strftime('%Y-%m-%d')

        schema = Scheme.objects.get_or_create(hall=hall, date=cur_day)[0]
        placings = Placing.objects.filter(scheme=schema)
        if not placings.count():
            for placing in Placing.objects.filter(date=prev_day, scheme__hall=hall):
                data = {
                    'date': cur_day,
                    'table': placing.table,
                    'coord_x': placing.coord_x,
                    'coord_y': placing.coord_y,
                    'scheme': schema
                }
                pl = Placing(**data)
                pl.save()

        reservation = Reservation.objects.filter(placing__in=placings).values_list('placing_id', flat=True)
        context['scheme'] = schema
        context['placings'] = placings
        context['reservation'] = reservation
        context['date_form'] = DateForm(initial={'date': cur_day})

        context['reservation_form'] = ReservationForm(initial={'date': cur_day, 'table': 0},
                                                      instance=schema)
        context['client_form'] = ClientForm(instance=schema)

        return context


class ReservationFormView(views.FormView):
    template_name = 'reservation/base.html'
    model = Reservation
    model_client = Client
    form_class = ReservationForm
    form_client_class = ClientForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if not hasattr(self, 'instance'):
            date = timezone.datetime.strptime(kwargs['data']['date'], '%d.%m.%Y').date()
            scheme = Scheme.objects.filter(hall_id=self.kwargs['pk'], date=date)[0]
            kwargs.update({'instance': scheme})

        return kwargs

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form(self.form_class)
        self.form_client = self.get_form(self.form_client_class)
        if form.is_valid():
            if self.form_client.is_valid():
                return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form_client = self.form_client
        table = form.cleaned_data['table']
        date = form.cleaned_data['date']
        hall = Hall.objects.filter(scheme__placing__table=table)[0]
        reservation_data = {
            'placing': Placing.objects.get(table=table, date=date),
            'date': date,
        }
        client, create = self.model_client.objects.get_or_create(email=form_client.cleaned_data['email'])
        client.name = form_client.cleaned_data['name']

        try:
            client.save()
        except Exception as e:
            messages.error(self.request, 'Ошибка при сохранении клиента {}'.format(e))
        else:
            reservation = self.model(**reservation_data)
            reservation.client = client
            try:
                reservation.save()
            except IntegrityError as e:
                messages.error(
                    self.request,
                    'Стол №{} на выбранную дату уже занят, выберите другой.'.format(table.table_number))
            except Exception as e:
                messages.error(
                    self.request,
                    'При сохранении заказа возникла непредвиденна ошибка. {}'.format(e))
            else:
                subject = 'Стол зарезервирован'
                to_email = form_client.cleaned_data['email']
                from_email = ADMIN_EMAIL

                html_content = render_to_string('reservation/messages/successfully_reserved.html',
                                                {'name': client.name,
                                                 'table': table.table_number,
                                                 'date': date})  # render with dynamic value
                text_content = strip_tags(
                    html_content)  # Strip the html tag. So people can see the pure text at least.

                # create the email, and attach the HTML version as well.
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
                msg.attach_alternative(html_content, "text/html")

                try:
                    msg.send()
                except Exception as e:
                    messages.warning(self.request,
                                     'Во время отправки сообщения произошла непредвиденная ошибка, попробуйте, '
                                     'пожалуйста позднее. {}'.format(e))
                else:
                    messages.success(self.request, 'Сообщение успешно отправлено')
                messages.info(self.request, 'Стол успешно зарезервирован')

        # redirect to same contact page with success message
        return HttpResponseRedirect(reverse('scheme', kwargs={'pk': hall.pk, 'date': date}))


class ChangeDateView(views.FormView):
    template_name = 'reservation/base.html'
    form_class = DateForm

    def post(self, request, *args, **kwargs):
        date = timezone.datetime.strptime(request.POST['date'], '%d.%m.%Y').date()
        hall = Hall.objects.filter(pk=kwargs['pk'])[0]

        return HttpResponseRedirect(reverse('scheme', kwargs={'pk': hall.pk, 'date': date.strftime('%Y-%m-%d')}))

