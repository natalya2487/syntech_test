from django.contrib import admin
from django.core.urlresolvers import reverse

from .models import Table, Hall, Scheme, Client, Reservation, Placing


class PlacingInline(admin.TabularInline):
    model = Placing


class SchemeAdmin(admin.ModelAdmin):
    inlines = [
        PlacingInline,
    ]

    list_display = ['id', 'date', 'hall', 'count_tables']
    list_filter = ['date', 'hall']
    list_per_page = 10
    search_fields = ['hall', 'date',]

    def view_on_site(self, obj):
        return reverse('scheme', kwargs={'pk':obj.hall.id, 'date': obj.date.strftime('%Y-%m-%d')})


class ReservationAdmin(admin.ModelAdmin):

    list_display = ['id', 'date', 'table', 'client']
    list_filter = ['date', 'placing__table']
    list_per_page = 10
    search_fields = []


class ClientAdmin(admin.ModelAdmin):

    list_display = ['name', 'email']
    list_filter = ['name']
    list_per_page = 10
    search_fields = ['email', 'name']


class HallAdmin(admin.ModelAdmin):

    list_display = ['name']
    list_per_page = 10
    search_fields = ['name']


class TableAdmin(admin.ModelAdmin):
    list_display = [ 'table_number', 'number_of_seats', 'shape', 'size_x', 'size_y']
    list_filter = ['shape', 'number_of_seats',]
    list_per_page = 10
    search_fields = ['table_number', 'shape']
    list_editable = ['number_of_seats', 'shape', 'size_x', 'size_y']
    ordering = ['table_number',]


for model, modelAdmin in ((Table, TableAdmin), (Hall, HallAdmin), (Scheme, SchemeAdmin), (Reservation, ReservationAdmin), (Client, ClientAdmin)):
    admin.site.register(model, modelAdmin)

