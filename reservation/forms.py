from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions

from .models import Table, Reservation, Client, Hall, Scheme


class ReservationForm(forms.Form):
    date = forms.DateField()
    table = forms.ModelChoiceField(queryset=Table.objects.all())

    def __init__(self, instance, *args, **kwargs):
        super(ReservationForm, self).__init__(*args, **kwargs)
        # self.table = forms.ModelChoiceField(queryset=kwargs['initial']['tables'])
        queryset = Table.objects.filter(placing__scheme=instance)
        reservations = Reservation.objects.filter(placing__scheme=instance)
        queryset.exclude(placing__reservation__in=reservations)
        self.fields['table'].queryset = queryset

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.labels_uppercase = True

        # set form field properties
        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-3 col-form-label'
        self.helper.field_class = 'col-sm-9'

        self.helper.layout = Layout(
            Field('date', wrapper_class='row'),
            Field('table', wrapper_class='row'),
        )

    def get_table_list(self):
        return self.tables


class ClientForm(forms.Form):
    name = forms.CharField(label='Имя')
    email = forms.EmailField(label='email')

    def __init__(self, *args, **kwargs):
        kwargs.pop('instance')
        super(ClientForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.form_tag = False
        self.helper.disable_csrf=True
        self.helper.labels_uppercase = True

        # set form field properties
        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-3 col-form-label'
        self.helper.field_class = 'col-sm-9'

        self.helper.layout = Layout(
            Field('name', wrapper_class='row'),
            Field('email', wrapper_class='row'),
        )


class DateForm(forms.Form):
    date = forms.DateField()

    def __init__(self, *args, **kwargs):
        super(DateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.form_method = 'POST'
        self.helper.form_tag = False
        self.helper.labels_uppercase = True

        # set form field properties
        self.helper.help_text_inline = True
        self.helper.html5_required = True
        self.helper.label_class = 'col-sm-3 col-form-label'
        self.helper.field_class = 'col-sm-8'

        self.helper.layout = Layout(
            Field('date', css_class='input-xlarge'),
        )
