"""Test Project Restaurant URL Configuration
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from reservation import views

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^hall/$', views.index, name='halls'),
    url(r'^hall/(?P<pk>[\d]+)/(?P<date>\d{4}-\d{2}-\d{2})/$', views.SchemeTemplateView.as_view(), name='scheme'),
    url(r'^hall/(?P<pk>[\d]+)/reservation$', views.ReservationFormView.as_view(), name='reservation'),
    url(r'^hall/(?P<pk>[\d]+)/change_date/$', views.ChangeDateView.as_view(), name='change_date'),

    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
